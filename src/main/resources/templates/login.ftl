<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
<div class="section">
<br />
<h4>Login page</h4>

<@l.login "/login" />

<a href="/registration">Add new user</a>

</div>
</@c.page>
