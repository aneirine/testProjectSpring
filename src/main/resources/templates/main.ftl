<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>


<@c.page>
<div>
<@l.logout />
</div>

<div class="section">
    <div>
        <form method="post">
            <input type="text" name="text" class="form-control" placeholder="Введите сообщение"/><br />
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button type="submit" class="btn">Добавить</button>
        </form>
    </div>
    <br />
    <div>Список сообщений</div>
    <form method="post" action="filter">
        <input type="text" name="filter" class="form-control"><br />
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button type="submit" class="btn">Найти</button>
    </form>
    <br />
    <#list messages as message>
    <div>
        <b>${message.id}</b>
        <span>${message.text}</span>
        <strong>${message.authorName}</strong>
        <strong>${message.date}</strong>
    <#else >
        No messages
    </div>
    </#list>
</div>
</@c.page>